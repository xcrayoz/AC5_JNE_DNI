﻿using AC5_JNE_DNI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AC5_JNE_DNI.Controllers
{
	[Route("~/ajax/jne")]
	public class AjaxJNEController : Controller
	{
		[HttpGet("[action]")]
		public async Task<IActionResult> ConsultaDNI(string dni)
		{
			var response = new ResponseEN();
			try
			{
				var client = new HttpClient();
				client.DefaultRequestHeaders.Add("Requestverificationtoken", AspNetCorePropertyShared.Requestverificationtoken);
				var pairs = new Dictionary<string, string> { { "CODDNI", dni } };
				var result = await client.PostAsync(AspNetCorePropertyShared.URL, new FormUrlEncodedContent(pairs));
				if (result.IsSuccessStatusCode)
				{
					var DATA = await result.Content.ReadAsStringAsync();
					response = System.Text.Json.JsonSerializer.Deserialize<ResponseEN>(DATA);
				}
			}
			catch (Exception ex)
			{
				response.mensaje = ex.Message;
			}
			return Json(response);

		}
	}
}
