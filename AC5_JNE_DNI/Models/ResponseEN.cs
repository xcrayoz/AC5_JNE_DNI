﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AC5_JNE_DNI.Models
{
	public class ResponseEN
	{
		public bool success { get; set; } = false;
		public string data { get; set; }
		public string mensaje { get; set; }
	}
}
